const gameContainer = document.getElementById("game");

const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);
        // console.log(index);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

// let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over

        newDiv.classList.add(color);
        newDiv.classList.add("hidden");
        newDiv.setAttribute("data-card", color);

        // call a function handleCardClick when a div is clicked on

        newDiv.addEventListener("click", handleCardClick);
        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}
let score = 0;

function resetGame() {
    alert("Congrulations You Won The Game");
    shuffledColors = shuffle(COLORS);
    score = 0;
    gameContainer.innerHTML = "";
    createDivsForColors(shuffledColors);
}
// TODO: Implement this function!
let hasFlippedCard = false;
let firstCard;
let secondCard;

function handleCardClick(e) {
    //if the class is there add it if not remove it
    this.classList.remove("hidden");
    if (!hasFlippedCard) {
        //first click
        hasFlippedCard = true;
        firstCard = this; // event.target
    } else {
        //second click
        hasFlippedCard = false;
        secondCard = this; //event.target
        checkForMatch(firstCard, secondCard);
        firstCard = undefined;
        secondCard = undefined;
    }
}

function checkForMatch(firstCard, secondCard) {
    if (
        firstCard.getAttribute("data-card") === secondCard.getAttribute("data-card")
    ) {
        console.log("matched");
        //if its a match
        disableCards(firstCard, secondCard);
    } else {
        //if it's not a match
        unFlippedCard(firstCard, secondCard);
    }
}

function disableCards(firstCard, secondCard) {
    console.log(firstCard, secondCard);
    score++;
    if (score === 5) {
        setTimeout(() => {
            resetGame();
        }, 500);
    }
    firstCard.removeEventListener("click", handleCardClick);
    secondCard.removeEventListener("click", handleCardClick);
}

function unFlippedCard(firstCard, secondCard) {
    setTimeout(() => {
        firstCard.classList.add("hidden");
        secondCard.classList.add("hidden");
    }, 500);
}

let shuffledColors = shuffle(COLORS);
createDivsForColors(shuffledColors);